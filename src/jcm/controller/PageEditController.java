package jcm.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.ThreadLocalRandom;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.stage.Stage;
import javafx.util.Pair;
import properties_manager.PropertiesManager;
import saf.controller.AppFileController;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppYesNoCancelDialogSingleton;
import static jcm.PropertyType.ADD_ELEMENT_ERROR_MESSAGE;
import static jcm.PropertyType.ADD_ELEMENT_ERROR_TITLE;
import static jcm.PropertyType.ATTRIBUTE_UPDATE_ERROR_MESSAGE;
import static jcm.PropertyType.ATTRIBUTE_UPDATE_ERROR_TITLE;
import static jcm.PropertyType.CSS_EXPORT_ERROR_MESSAGE;
import static jcm.PropertyType.CSS_EXPORT_ERROR_TITLE;
import jcm.JClassModerer;
import jcm.data.DataManager;
import jcm.data.UMLClassPrototype;
import jcm.data.UMLVariablePrototype;
import jcm.file.FileManager;
import jcm.gui.Workspace;
import saf.components.AppPageEditComponent;
import static saf.settings.AppPropertyType.PROPERTIES_LOAD_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.PROPERTIES_LOAD_ERROR_TITLE;
import saf.ui.AppGUI;

/**
 * This class provides event programmed responses to workspace interactions for
 * this application for things like adding elements, removing elements, and
 * editing them.
 *
 * @author Richard McKenna
 * @author Ziling Zhou
 * @version 1.0
 */
public class PageEditController implements AppPageEditComponent {

    // HERE'S THE FULL APP, WHICH GIVES US ACCESS TO OTHER STUFF
    JClassModerer app;
    double initX = 0,initY = 0,dx = 0,dy = 0,newX, newY;
    private boolean selectionMode;
    private VBox currentSelect;
    private UMLClassPrototype currentUML;
    
    /**
     * Constructor for initializing this object, it will keep the app for later.
     *
     * @param initApp The JavaFX application this controller is associated with.
     */
    public PageEditController(JClassModerer initApp) {
	// KEEP IT FOR LATER
	app = initApp;
    }
    @Override
    public void handleAddClassRequest(boolean isInterface){ 
      //  checkClassPackageNameCollision();
        
        DataManager dataManager = (DataManager)app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager.totalInit++;
        UMLClassPrototype umlCP = new UMLClassPrototype();
        dataManager.addNewPackage(umlCP);
            
        umlCP.setIsIneterface(isInterface);
        umlCP.initBox();
        foolProof();
        //mark the current select
        currentSelect = umlCP.getContainer();
        currentUML = umlCP;
        mark();
        
        workspace.getDesignRenderer().getChildren().add(umlCP.getContainer());
        CompoentToolbarView(umlCP);
        
   //     System.out.println(dataManager.totalClassN());
        umlCP.getContainer().setOnMouseClicked(e -> selectElement(e,umlCP));
        umlCP.getContainer().setOnMouseDragged(e -> selectElement(e,umlCP));
        umlCP.getContainer().setOnMousePressed(e -> selectElement(e,umlCP));
        emptyCollision();
        boolean col = dataManager.isCollide(umlCP);
        if(col)
            showCollision();
    }
    
    public void selectElement(MouseEvent e,UMLClassPrototype umlCP){
        if(selectionMode == false)
            return;
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
        
        
            if(e.getEventType() == MouseEvent.MOUSE_DRAGGED){
                    unmark();
                    currentSelect = umlCP.getContainer();
                    currentUML = umlCP;
                    mark();
                    CompoentToolbarView(umlCP);   
                
                dx = e.getX() - initX;
                dy = e.getY() - initY;
                newX = currentUML.getContainer().getLayoutX() + dx;
                newY = currentUML.getContainer().getLayoutY() + dy;
                currentUML.getContainer().setLayoutX(newX);
                currentUML.getContainer().setLayoutY(newY);
                currentUML.setxLocation(newX);
                currentUML.setyLocation(newY);
            }
            else if(e.getEventType() == MouseEvent.MOUSE_CLICKED){
                    unmark();
                    currentSelect = umlCP.getContainer();
                    currentUML = umlCP;
                    mark();
                    CompoentToolbarView(umlCP);
        
                    //TODO foolproof design including resize, remove, and undo, etc.
            }else if (e.getEventType() == MouseEvent.MOUSE_PRESSED){
                initX = e.getX();
                initY = e.getY();
            }
        
        workspace.getGui().updateToolbarControls(false);
   //     System.out.println(currentPNW);
        emptyCollision();
        boolean col = dataManager.isCollide(umlCP);
        if(col)
            showCollision();
    }
    
    public void foolProof(){
        DataManager dataManager = (DataManager)app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        AppGUI gui = app.getGUI();
        unmark();
        selectionMode = false;
        gui.updateFoolProof();
        
        if(dataManager.getAllData().size() > 0)
            gui.getSelect().setDisable(false);
        
        workspace.getClassNameTxt().setDisable(true);
        workspace.getPackageNameTxt().setDisable(true);
        emptyCompoentToolbarView();
        /*     select.setDisable(true);
        remove.setDisable(true);
        undo.setDisable(true);
        redo.setDisable(true);
        */
    }
    
    @Override
    public void handleSelectElementRequest(){
    //    checkClassPackageNameCollision();
        AppGUI gui = app.getGUI();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        foolProof();
        selectionMode = true;
        currentSelect = null;
        currentUML = null;
        gui.getSelect().setDisable(true);
        workspace.getWorkspace().setCursor(javafx.scene.Cursor.DEFAULT);
    }
    public void mark(){
        if(currentSelect != null){
            currentSelect.getStyleClass().clear();
            currentSelect.getStyleClass().add("uml_marked");
        }
    }
    public void unmark(){
        if(currentSelect != null){
            currentSelect.getStyleClass().clear();
            currentSelect.getStyleClass().add("uml_container_pane");    
        }
    }
    public void CompoentToolbarView(UMLClassPrototype umlCP){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getClassNameTxt().setText(umlCP.getClassName());
        workspace.getPackageNameTxt().setText(umlCP.getPackageName());
        workspace.getClassNameTxt().setDisable(false);
        workspace.getPackageNameTxt().setDisable(false);
    }
    /*
    * Empty workspace textField
    */
    public void emptyCompoentToolbarView(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getClassNameTxt().setText(null);
        workspace.getPackageNameTxt().setText(null);
        //todo setDisable
    }
    public void updateUMLClassName(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        //workspace.getClassNameTxt().getStyleClass().clear();
        
        currentUML.setClassName(workspace.getClassNameTxt().getText());
        currentUML.getTop().getChildren().clear();
        currentUML.getTop().getChildren().add(new Label(currentUML.getClassName()));
        
        emptyCollision();
        
        boolean collision = dataManager.isCollide(currentUML); 
        if(collision){
            showCollision();
 //           System.out.println("Collision");
        }
        currentUML.setNamingCollision(collision);
    //        System.out.println("This package class n =" + currentArr.size());
    //        System.out.println(dataManager.totalClassN());
   //     dataManager.totalClassN();
    }//TODO need to update the string package name in the packageWrap class
    public void updateUMLPackageName(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent(); 
        boolean collision = false;
        boolean merge = dataManager.attempToMerge(workspace.getPackageNameTxt().getText(), currentUML);
        
        emptyCollision();
        
        dataManager.removeClass(currentUML);
        currentUML.setPackageName(workspace.getPackageNameTxt().getText());
        //failing of merge imply that no previously existing package, and we just have to change package name
        if(merge != true){
            dataManager.addNewPackage(currentUML);
        }else{
        //check for collision of naming 
        //new key should be checked currentPNW is being removed, should replace current PNW with new entry key
            collision = dataManager.isCollide(currentUML); 
            if(collision){
                showCollision();
            }
        }
        currentUML.setNamingCollision(collision);
   //     System.out.println("true size: " + dataManager.totalClassN());
    }
    public void emptyCollision(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getClassNameTxt().getStyleClass().clear();
        workspace.getClassNameTxt().getStyleClass().add("prompt_text_field");
        workspace.getPackageNameTxt().getStyleClass().clear();
        workspace.getPackageNameTxt().getStyleClass().add("prompt_text_field");
    }
    public void showCollision(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getClassNameTxt().getStyleClass().clear();
        workspace.getClassNameTxt().getStyleClass().add("alert");
        workspace.getPackageNameTxt().getStyleClass().clear();
        workspace.getPackageNameTxt().getStyleClass().add("alert");
    }
    /**
    @param tb the variable table 
    */
    public void handleAddVar(TableView<UMLVariablePrototype> tb){
    //    currentUML.getVariable().add(new UMLVariablePrototype());
    //    List<UMLVariablePrototype> varL = currentUML.getVariable();
    //    List li = new List();
        final ObservableList<UMLVariablePrototype> lis 
                = FXCollections.observableArrayList();
        lis.add(new UMLVariablePrototype("Dummy","int",false,"public"));
        lis.add(new UMLVariablePrototype("Dummy","int",false,"public"));
    //    TableRow r = new TableRow();
        tb.setItems(lis);
//    tb.getItems().add(varL);
    }
    
    public void handleRemoveVar(TableView<UMLVariablePrototype> tb){
       ObservableList<UMLVariablePrototype> item = tb.getItems();
       ObservableList<UMLVariablePrototype> sel = tb.getSelectionModel().getSelectedItems();
       for(UMLVariablePrototype u: sel){
           item.remove(u);
       }
    }
    
    public void handleVName(Event e){
        TableColumn.CellEditEvent<UMLVariablePrototype, String> ce;
        ce = (TableColumn.CellEditEvent<UMLVariablePrototype, String>) e;
        UMLVariablePrototype u = ce.getRowValue();
        u.setName(ce.getNewValue());
    }
    
}
