package jcm.gui;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import saf.ui.AppYesNoCancelDialogSingleton;
import saf.ui.AppMessageDialogSingleton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import jcm.PropertyType;
import static jcm.PropertyType.TEMP_PAGE_LOAD_ERROR_MESSAGE;
import static jcm.PropertyType.TEMP_PAGE_LOAD_ERROR_TITLE;
import static jcm.PropertyType.UPDATE_ERROR_MESSAGE;
import static jcm.PropertyType.UPDATE_ERROR_TITLE;
import jcm.JClassModerer;
import static jcm.PropertyType.ADD_ROW_ICON;
import static jcm.PropertyType.ADD_ROW_TOOLTIP;
import static jcm.PropertyType.REMOVE_ROW_ICON;
import static jcm.PropertyType.REMOVE_ROW_TOOLTIP;
import jcm.controller.PageEditController;
import jcm.data.DataManager;
import jcm.data.UMLClassPrototype;
import jcm.data.UMLMethodPrototype;
import jcm.data.UMLVariablePrototype;
import jcm.file.FileManager;


/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */

public class Workspace extends AppWorkspaceComponent {
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    // THIS HANDLES INTERACTIONS WITH PAGE EDITING CONTROLS
    PageEditController pageEditController;

    // WE'LL PUT THE WORKSPACE INSIDE A SPLIT PANE
    BorderPane workspaceBorderPane;

    //UI we will use to set up the workspaces
    ScrollPane scrollDR,scrollT1,scrollT2;
    
    Pane DesignRenderer;
    Pane compoentToolbar;
        GridPane g1;
            Label classNameLbl;
            Label packageNameLbl;
            Label parentNameLbl;
            TextField classNameTxt,packageNameTxt;
            ComboBox parentCB;
        
        VBox vbc; 
        VBox vb1;
        HBox hb1;
            Label variableLbl;
            Button addRowBtn,removeRowBtn;
            TableView<UMLVariablePrototype> variableTable;
            TableColumn<UMLVariablePrototype,String> Vname;
            TableColumn<UMLVariablePrototype,String> Vtype;
            TableColumn<UMLVariablePrototype,Boolean> VstaticYN;
            TableColumn<UMLVariablePrototype,String> Vaccess;
            
        VBox vb2;
        HBox hb2;
            Label methodLbl;
            Button addRowBtnC,removeRowBtnC;
            TableView methodTable;
            TableColumn<UMLMethodPrototype,String> Mname;
            TableColumn<UMLMethodPrototype,String> Mtype;
            TableColumn<UMLMethodPrototype,String> MstaticYN;
            TableColumn<UMLMethodPrototype,String> Maccess;
            TableColumn<UMLMethodPrototype,String> Mabstract;
            TableColumn<UMLMethodPrototype,String> Marg1;
            TableColumn<UMLMethodPrototype,String> Marg2;
            TableColumn<UMLMethodPrototype,String> Marg3;

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();

	// THIS WILL PROVIDE US WITH OUR CUSTOM UI SETTINGS AND TEXT
	PropertiesManager propsSingleton = PropertiesManager.getPropertiesManager();

	// WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
	workspace = new Pane();

        workspaceBorderPane = new BorderPane();
        workspaceBorderPane.prefHeightProperty().bind(workspace.heightProperty());
        workspaceBorderPane.prefWidthProperty().bind(workspace.widthProperty());
        
        layoutGUI();
	setupHandlers();
        
        workspace.getChildren().add(workspaceBorderPane);
        
	// THIS WILL MANAGE ALL EDITING EVENTS
//	pageEditController = new PageEditController((JClassModerer) app);
	// LOAD ALL THE HTML TAG TYPES
	FileManager fileManager = (FileManager) app.getFileComponent();
	DataManager dataManager = (DataManager) app.getDataComponent();

    }
    
    private void layoutGUI(){
        DesignRenderer = new Pane();
        scrollDR = new ScrollPane();
        scrollDR.setContent(getDesignRenderer());
        scrollDR.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scrollDR.setHbarPolicy(ScrollBarPolicy.ALWAYS);
 //       scrollDR.setFitToHeight(true);
 //       scrollDR.setFitToWidth(true);
       
        workspaceBorderPane.setCenter(scrollDR);
        
        g1 = new GridPane();
        
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(50);
        g1.getColumnConstraints().addAll(column1, column2); // each get 50% of width
       
        classNameLbl = new Label("Class Name:");
        packageNameLbl = new Label("Package:");
        parentNameLbl = new Label("parent:");
        g1.addColumn(0, classNameLbl,packageNameLbl,parentNameLbl);
        
        classNameTxt = new TextField();
        classNameTxt.setMinHeight(35);
        classNameTxt.setDisable(true);
        packageNameTxt = new TextField();
        packageNameTxt.setMinHeight(35);
        packageNameTxt.setDisable(true);
        parentCB = new ComboBox();
        parentCB.prefWidthProperty().bind(packageNameTxt.widthProperty());
        g1.addColumn(1,classNameTxt,packageNameTxt,parentCB);
  //      g1.setPadding(new Insets(5, 0, 5, 0));
        
        vb1 = new VBox(5);
        hb1 = new HBox(5);
        variableLbl = new Label("Variables:     ");
        hb1.getChildren().add(variableLbl);
        addRowBtn = gui.initChildButton(hb1, ADD_ROW_ICON.toString(), ADD_ROW_TOOLTIP.toString(), false);
        removeRowBtn = gui.initChildButton(hb1, REMOVE_ROW_ICON.toString(), REMOVE_ROW_TOOLTIP.toString(), false);
        variableTable = new TableView();
            Vname = new TableColumn("Name");
            Vname.setPrefWidth(150);
            Vname.setCellValueFactory(new PropertyValueFactory("Name"));
            Vname.setCellFactory(TextFieldTableCell.forTableColumn());
            
            
            Vtype = new TableColumn("Type");
            Vtype.setCellValueFactory(new PropertyValueFactory("Type"));
            Vtype.setCellFactory(TextFieldTableCell.forTableColumn());
            
            VstaticYN = new TableColumn("Static");
            VstaticYN.setCellValueFactory(new PropertyValueFactory("Static"));
            Vtype.setCellFactory(CheckBoxTableCell.forTableColumn(VstaticYN));
            
            Vaccess = new TableColumn("Access");
            Vaccess.setCellValueFactory(new PropertyValueFactory("Access"));
            Vaccess.setCellFactory(TextFieldTableCell.forTableColumn());
            
        variableTable.getColumns().setAll(Vname,Vtype,VstaticYN,Vaccess);
        variableTable.setEditable(true);
        
        
        
        scrollT1 = new ScrollPane();
        scrollT1.setContent(variableTable);
        scrollT1.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scrollT1.setHbarPolicy(ScrollBarPolicy.ALWAYS);
 //       scrollT1.setFitToHeight(true);
 //       scrollT1.setFitToWidth(true);
        vb1.getChildren().addAll(hb1,scrollT1);
        
        vb2 = new VBox(5);
        hb2 = new HBox(5);
        methodLbl = new Label("Methods:     ");
        addRowBtnC = gui.initChildButton(hb1, ADD_ROW_ICON.toString(), ADD_ROW_TOOLTIP.toString(), false);
        removeRowBtnC = gui.initChildButton(hb1, REMOVE_ROW_ICON.toString(), REMOVE_ROW_TOOLTIP.toString(), false);
        hb2.getChildren().addAll(methodLbl,addRowBtnC,removeRowBtnC);
        methodTable = new TableView();
            Mname = new TableColumn("Name");
            Mname.setPrefWidth(150);
            Mname.setCellFactory(TextFieldTableCell.forTableColumn());
            Mname.setCellValueFactory(new PropertyValueFactory("Name"));
 
            Mtype = new TableColumn("Return");
            Mtype.setCellValueFactory(new PropertyValueFactory("Return"));
            
            MstaticYN = new TableColumn("Static");
            
            Maccess = new TableColumn("access");
            
            Mabstract= new TableColumn("Abstract");
            
            Marg1 = new TableColumn("Arg1");
            
            Marg2 = new TableColumn("Arg2");
            
            Marg3 = new TableColumn("Arg3");
            
            
        methodTable.getColumns().setAll(Mname,Mtype,MstaticYN,Mabstract,Maccess,Marg1,Marg2,Marg3);
        scrollT2 = new ScrollPane();
        scrollT2.setContent(methodTable);
        scrollT2.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scrollT2.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        scrollT2.setFitToHeight(true);
    //    scrollT2.setFitToWidth(true);
        vb2.getChildren().addAll(hb2,scrollT2);
        
        vbc = new VBox();
        vbc.getChildren().addAll(vb1,vb2);
        compoentToolbar = new VBox();
        getCompoentToolbar().getChildren().addAll(g1,vbc);
        //setting width and height property\
        
    //    DesignRenderer.setMaxWidth(workspace.getWidth()-compoentToolbar.getWidth());
        
        getCompoentToolbar().setMaxWidth(400);
        getCompoentToolbar().setPrefWidth(400);
        getCompoentToolbar().prefHeightProperty().bind(workspace.heightProperty());
        scrollDR.prefHeightProperty().bind(workspace.heightProperty());
        scrollDR.prefWidthProperty().bind(workspace.widthProperty().subtract(getCompoentToolbar().widthProperty()));
        getDesignRenderer().prefHeightProperty().bind(scrollDR.heightProperty());
        getDesignRenderer().prefWidthProperty().bind(scrollDR.widthProperty());
    
        workspaceBorderPane.setRight(getCompoentToolbar());
        
    }
    
    private void setupHandlers(){
        PageEditController pec = (PageEditController)app.getPageEditComponent();
        classNameTxt.setOnKeyReleased(e -> pec.updateUMLClassName());
        packageNameTxt.setOnKeyReleased(e -> pec.updateUMLPackageName());
        addRowBtn.setOnAction(e -> pec.handleAddVar(variableTable));
        removeRowBtn.setOnAction(e -> pec.handleRemoveVar(variableTable));
        Vname.setOnEditCommit(e -> pec.handleVName(e));
        
    }
    
    
    
    
    /**
    * Accessor Method for getting app gui
    * @return the gui of editing work
    */
    public AppGUI getGui(){
        return gui;
    }
    
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
//        workspace.getStyleClass().add("fit_pane");
        getDesignRenderer().getStyleClass().add("designRender_pane");
        getCompoentToolbar().getStyleClass().add("compoentToolbar_pane");
        
        classNameLbl.getStyleClass().add("subheading_label");
        packageNameLbl.getStyleClass().add("subheading_label");
        parentNameLbl.getStyleClass().add("subheading_label");
        variableLbl.getStyleClass().add("subheading_label");
        methodLbl.getStyleClass().add("subheading_label");

        classNameTxt.getStyleClass().add("prompt_text_field");
        packageNameTxt.getStyleClass().add("prompt_text_field");        
                
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        DesignRenderer.getChildren().clear();
        packageNameTxt.setText(null);
        classNameTxt.setText(null);
        PageEditController pec = (PageEditController)app.getPageEditComponent();
        DataManager dm = (DataManager)app.getDataComponent();
        HashMap<String,ArrayList<UMLClassPrototype>> hl = dm.getAllData();
        for (Map.Entry<String,ArrayList<UMLClassPrototype>> entry : hl.entrySet()) {
             String key = entry.getKey();
             ArrayList<UMLClassPrototype> value = entry.getValue();
             System.out.println(value.size());
             for(int i = 0; i < value.size();i++){
                UMLClassPrototype uc = value.get(i);
                //value.get(i).fullFillLayout();
                uc.fullFillLayout();
                VBox vb = value.get(i).getContainer();
                DesignRenderer.getChildren().add(vb);
                vb.setOnMouseClicked(e -> pec.selectElement(e,uc));
                vb.setOnMouseDragged(e -> pec.selectElement(e,uc));
                vb.setOnMousePressed(e -> pec.selectElement(e,uc));
             }
        }
        pec.foolProof();
    }

    /**
     * @return the DesignRenderer
     */
    public Pane getDesignRenderer() {
        return DesignRenderer;
    }

    /**
     * @return the compoentToolbar
     */
    public Pane getCompoentToolbar() {
        return compoentToolbar;
    }

    /**
     * @return the classNameTxt
     */
    public TextField getClassNameTxt() {
        return classNameTxt;
    }

    /**
     * @return the packageNameTxt
     */
    public TextField getPackageNameTxt() {
        return packageNameTxt;
    }
}
