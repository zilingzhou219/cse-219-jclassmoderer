package jcm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import jcm.JClassModerer;
import jcm.data.DataManager;
import jcm.data.UMLClassPrototype;
import jcm.data.UMLMethodPrototype;
import jcm.data.UMLVariablePrototype;



/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author Ziling Zhou
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    
    //For Json writing and reading
    public static final String CLASSNAME = "className";
    public static final String PACKAGENAME = "packageName";
    public static final String PARENTNAME = "parentName";
    public static final String ISINTERFACE = "isInterface";
    public static final String XLOCATION = "xLocation";
    public static final String YLOCATION = "yLocation";
    public static final String HEIGHT = "height";
    public static final String WIDTH = "width";
    public static final String METHOD = "method";
    public static final String CONNECTER = "connecter";
        //|||||||||||||||||||||||||||||||||
        public static final String METHODNAME = "methodName";
        public static final String ACCESS = "access";
        public static final String ISABSTRACT = "isAbstract";
        public static final String ARGLIST = "argList";
            //|||||||||||||||||||||||||||||
            public static final String VARIABLE = "variable";
            public static final String VARIABLENAME = "variableName";
            public static final String ISSTATIC = "isStatic";
            public static final String TYPE = "type";

    JClassModerer app;
    public FileManager(AppTemplate initApp){
        app = (JClassModerer)initApp;
    }
    public FileManager(){
        
    }
    
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        
        filePath += ".json";
	// BUILD THE HTMLTags ARRAY
	DataManager dataManager = (DataManager)data;
        HashMap<String,ArrayList<UMLClassPrototype>> allD = dataManager.getAllData();
        if(allD.size() < 1)
            return;
        
        JsonObjectBuilder jsoLv1Builder = Json.createObjectBuilder();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	for (Map.Entry<String, ArrayList<UMLClassPrototype>> entry : allD.entrySet()) {
             String key = entry.getKey();
             ArrayList<UMLClassPrototype> value = entry.getValue();
             JsonArray Apackage = buildPackage(value);
             jsoLv1Builder.add(key, Apackage);
        }
        
        JsonObject dataManagerJSO = jsoLv1Builder.build();

	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();

    }
    public JsonArray buildPackage(ArrayList<UMLClassPrototype> arr){
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for(int i = 0; i < arr.size(); i++){
            JsonObject classObject = buildClass(arr.get(i));
            builder.add(classObject);
        }
        return builder.build();
    }
    public JsonObject buildClass(UMLClassPrototype umlC){
        JsonObjectBuilder ob = Json.createObjectBuilder()
                .add(CLASSNAME,umlC.getClassName())
                .add(PACKAGENAME, umlC.getPackageName())
                .add(PARENTNAME, umlC.getParentName())
                .add(ISINTERFACE,umlC.isIsIneterface())
                .add(ISABSTRACT,umlC.isIsAbstract())
                .add(XLOCATION, umlC.getxLocation())
                .add(YLOCATION,umlC.getyLocation())
                .add(HEIGHT,umlC.getHeight())
                .add(WIDTH,umlC.getWidth())
                .add(CONNECTER,buildConnecter(umlC.getConnector()));
    //    if(umlC.getMethod().size() > 0)
                ob.add(METHOD,buildMethod(umlC.getMethod()));
    //    if(umlC.getVariable().size() > 0)
                ob.add(VARIABLE,buildVariable(umlC.getVariable()));
                
        
        return ob.build();        
    }
    public JsonArray buildMethod(ArrayList<UMLMethodPrototype> umlM){
        JsonArrayBuilder ab = Json.createArrayBuilder();
        for(int i = 0; i < umlM.size(); i++){
            ab.add(buildMethod(umlM.get(i)));
        }
        return ab.build();
    }
    public JsonObject buildMethod(UMLMethodPrototype umlM){
        JsonObjectBuilder ob = Json.createObjectBuilder()
                .add(METHODNAME,umlM.getName())
                .add(TYPE,umlM.getType())
                .add(ACCESS, umlM.getAccess())
                .add(ISSTATIC,umlM.isIsStatic())
                .add(ISABSTRACT,umlM.isIsAbstract())
                .add(ARGLIST, buildArgList(umlM.getArgList()));
        return ob.build();
    }
    public JsonArray buildArgList(ArrayList<String> arr){
        JsonObjectBuilder ob = Json.createObjectBuilder();
        JsonArrayBuilder ab = Json.createArrayBuilder();
        for(int i = 0; i < arr.size(); i++){
            ab.add(arr.get(i));
        } 
        return ab.build();
    }
    
    public JsonArray buildVariable(ArrayList<UMLVariablePrototype> umlV){
        JsonArrayBuilder ab = Json.createArrayBuilder();
        for(int i = 0; i < umlV.size(); i++){
            ab.add(buildV(umlV.get(i)));
        }
        return ab.build();
    }
    public JsonObject buildV(UMLVariablePrototype umlV){
        JsonObjectBuilder ob = Json.createObjectBuilder()
                .add(VARIABLENAME,umlV.getName())
                .add(TYPE,umlV.getType())
                .add(ACCESS, umlV.getAccess())
                .add(ISSTATIC,umlV.isIsStatic());

        return ob.build();
    }
    public JsonArray buildConnecter(Polygon po){
        Iterator it = po.getPoints().iterator();
        JsonArrayBuilder ob = Json.createArrayBuilder();
        while(it.hasNext())
            ob.add((double)it.next());
        
        return ob.build();
    }
    
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        
        DataManager dm = (DataManager)data;
        JsonObject jso = loadJSONFile(filePath);
        dm.getAllData().clear();
//        for(int i = 0; i < jso.size(); i++){
//            JsonObject arr = jso.
//        }
        for (Map.Entry entry : jso.entrySet()) {
            //the package name 
            String key = (String)entry.getKey();
            //the assciated classes
            JsonArray arr = (JsonArray)entry.getValue();
            ArrayList<UMLClassPrototype> hashL = new ArrayList<>();
            
            for(int i = 0; i< arr.size(); i++){
                JsonObject eachC = arr.getJsonObject(i);
                String classN = eachC.getString(CLASSNAME);
                String packN = eachC.getString(PACKAGENAME);
                String parent = eachC.getString(PARENTNAME);
                boolean isI = eachC.getBoolean(ISINTERFACE);
                boolean isA = eachC.getBoolean(ISABSTRACT);
                double x = eachC.getInt(XLOCATION);
                double y = eachC.getInt(YLOCATION);
                double h = eachC.getInt(HEIGHT);
                double w = eachC.getInt(WIDTH);
                
                UMLClassPrototype ec = new UMLClassPrototype();
                ec.setClassName(classN);
                ec.setPackageName(packN);
                ec.setParentName(parent);
                ec.setIsIneterface(isI);
                ec.setIsAbstract(isA);
                ec.setxLocation(x);
                ec.setyLocation(y);
                ec.setHeight(h);
                ec.setWidth(w);
                
                
                Polygon pol = new Polygon();
                JsonArray ct =eachC.getJsonArray(CONNECTER);
                for(int cti = 0; cti < ct.size(); cti++){
                    pol.getPoints().add((double)ct.getInt(cti));
                }
                ec.setConnector(pol);
                
                JsonArray methodA = eachC.getJsonArray(METHOD);
                for(int ma = 0; ma < methodA.size(); ma++){
                    JsonObject eachM = methodA.getJsonObject(ma);
                    String mn = eachM.getString(METHODNAME);
                    String ty = eachM.getString(TYPE);
                    String ac = eachM.getString(ACCESS);
                    boolean isS = eachM.getBoolean(ISSTATIC);
                    boolean isAB = eachM.getBoolean(ISABSTRACT);
                    JsonArray argL = eachM.getJsonArray(ARGLIST);
                    ArrayList<String> argS = new ArrayList();
                    for(int argLi = 0; argLi < argL.size(); argLi++){
                        argS.add(argL.getString(argLi));
                    }
                    UMLMethodPrototype em = new UMLMethodPrototype();
                    em.setName(mn);
                    em.setType(ty);
                    em.setAccess(ac);
                    em.setIsStatic(isS);
                    em.setIsAbstract(isAB);
                    em.setArgList(argS);
                    ec.getMethod().add(em);
                }
                
                JsonArray varA = eachC.getJsonArray(VARIABLE);
                for(int vari = 0; vari < varA.size(); vari++){
                    JsonObject eachV = varA.getJsonObject(vari);
                    String vn = eachV.getString(VARIABLENAME);
                    String ty = eachV.getString(TYPE);
                    String ac = eachV.getString(ACCESS);
                    boolean isS = eachV.getBoolean(ISSTATIC);
                    
                    UMLVariablePrototype ev = new UMLVariablePrototype();
                    ev.setName(vn);
                    ev.setType(ty);
                    ev.setAccess(ac);
                    ev.setIsStatic(isS);
                    ec.getVariable().add(ev);
                    
                }
                hashL.add(ec);
              
                
            }
       //     System.out.println(hashL.size());
            dm.getAllData().put(key, hashL);
        //     System.out.println(key);
        //     System.out.println(arr.size());
        }
        
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        DataManager dm = (DataManager)data;
        String[] strA = filePath.split("work");
        
        strA[1] = strA[1].substring(1);
  //      System.out.println(strA[0]);
  //      System.out.println(strA[1]);
     //   PrintWriter writer = new PrintWriter(filePath);
//        System.out.println(filePath);
//        String str = "work/Try";
//        File file = new File(str);
//        file.mkdir();
          String combin = "work/"  + strA[1];
     //   System.out.println(conbin);
        File file2 = new File(combin);
        file2.mkdirs();
//        try{
//            if(file.mkdir()){ 
//            System.out.println("Directory Created");
//        } else {
//            System.out.println("Directory is not created");
//        }
//        } catch(Exception e){
//            e.printStackTrace();
//        } 
        //file.mkdir();
      //  writer.write("sadsasa");
        HashMap<String,ArrayList<UMLClassPrototype>> hm = dm.getAllData();
    //    System.out.println(hm.size());
        for (Map.Entry<String, ArrayList<UMLClassPrototype>> entry : hm.entrySet()) {
             String key = entry.getKey();
             ArrayList<UMLClassPrototype> value = entry.getValue();
             String[] nestedP = new String[10];
             ArrayList<String> list = new ArrayList<>();
             String nestF = "";
             if(key.contains(".")){
                 key = key.replaceAll("[.]", "/");
                 nestF = key;
//                 System.out.println(key);
//                nestedP = key.split(".");
//                System.out.println("length: " +nestedP.length);
//                System.out.println(nestedP);
//                for(int i = 0; i < nestedP.length; i++){
//                    nestF += nestedP[i] + "/";
//                    System.out.println("here" + nestedP[i]);
//                    System.out.println(nestF);
//                }
//                nestF = nestF.substring(0,nestF.length()-1);
            }
            else
                nestF = key;
           
  //           System.out.println(nestF);
             File file3 = new File(combin + "/" + nestF);
             file3.mkdirs();
              
             for(int i = 0; i < value.size();i++){
                PrintWriter writer = new PrintWriter(combin + "/" + nestF + "/" + value.get(i).getClassName()+".java"); 
                
                writer.write(value.get(i).toClassCode());
                writer.close();
             }
        }
             
        
    }
    
    @Override
    public void saveAsPhoto(AppDataComponent data, String filePath) throws IOException {

    }
 

}
