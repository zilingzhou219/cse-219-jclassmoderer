/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcm.data;

/**
 *
 * @author ziling
 */
public class UMLVariablePrototype {
    String name = "",type = "", access = "";
    boolean isStatic = false;
    public static String forImport = "";
    
    public UMLVariablePrototype(){
        
    }
    public UMLVariablePrototype(String name,String type,boolean isStatic,String access){
        this.name = name;
        this.type = type;
        this.access = access;
        this.isStatic = isStatic;
    }
    
    
    public String toString(){
        String str = "";
        str += "VariableName: " + name +"\n"
                +"type: "+type+"\n"
                +"access: " + access+"\n"
                +"isStatic " + isStatic+"\n";
              
              return str;
    }
    
    public String toCode(){
        String str = "";
        str = access;
        if(isStatic)
            str += " static";
        str += " " + type + " " + name + ";";
        return str;
    }
    
    public void forImport(){
        if(DataManager.allCoreClass.get(type) != null)
            forImport += DataManager.allCoreClass.get(type)+ "/n";
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the access
     */
    public String getAccess() {
        return access;
    }

    /**
     * @param access the access to set
     */
    public void setAccess(String access) {
        this.access = access;
    }

    /**
     * @return the isStatic
     */
    public boolean isIsStatic() {
        return isStatic;
    }

    /**
     * @param isStatic the isStatic to set
     */
    public void setIsStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }
    
    
}
