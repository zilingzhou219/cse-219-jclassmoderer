package jcm.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import saf.components.AppDataComponent;
import saf.AppTemplate;

import jcm.file.FileManager;
import jcm.gui.Workspace;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author Ziling Zhou
 * @version 1.0
 */
public class DataManager implements AppDataComponent {

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    public static int totalInit = 0;
    HashMap<String,ArrayList<UMLClassPrototype>> allData = new HashMap<>();
    public static HashMap<String,String> allCoreClass = new HashMap<>();
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp){
	// KEEP THE APP FOR LATER
	app = initApp;
        addCoreClass();
    }
    public DataManager(){
        addCoreClass();
    }
    private void addCoreClass(){
        allCoreClass.put("Button","javafx.scene.control.Button");
        allCoreClass.put("ScrollPane","javafx.scene.control.Button");
        allCoreClass.put("Task","javafx.concurrent.Task");
        allCoreClass.put("ScrollPane","javafx.scene.control.ScrollPane");
        allCoreClass.put("TextArea","javafx.scene.control.TextArea");
        allCoreClass.put("BorderPane","javafx.scene.layout.BorderPane");
        allCoreClass.put("FlowPane","javafx.scene.layout.FlowPane");
        allCoreClass.put("FlowPane","javafx.scene.layout.FlowPane");
        allCoreClass.put("Stage","javafx.stage.Stage");
        
    }
    
    public void addNewPackage(UMLClassPrototype umlC){
        ArrayList<UMLClassPrototype> arrlist = new ArrayList<>();
        arrlist.add(umlC);
        allData.put(umlC.getPackageName(), arrlist);
    }
    
    
    public void removeClass(UMLClassPrototype umlC){
        String tempP = umlC.getPackageName();
        for (Map.Entry<String, ArrayList<UMLClassPrototype>> entry : allData.entrySet()) {
             String key = entry.getKey();
             ArrayList<UMLClassPrototype> value = entry.getValue();
             if(tempP.equals(key)){
                 value.remove(umlC);
                 break;
        //         System.out.println("merged");
            }
        }
        if(allData.get(tempP).size() == 0)
            allData.remove(tempP);
    }
    
    public boolean attempToMerge(String pnwNew, UMLClassPrototype umlC){
        boolean merge = false;
        for (Map.Entry<String, ArrayList<UMLClassPrototype>> entry : allData.entrySet()) {
             String key = entry.getKey();
             ArrayList<UMLClassPrototype> value = entry.getValue();
             if(pnwNew.equals(key)){
                 value.add(umlC);
                 merge = true;
                 break;
            }
        }
       return merge; 
    }
    
    
    
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        allData = new HashMap<>();
        totalInit = 0;
    }
    public int totalClassN(){
        //Iterator it = classPackageName.entrySet().iterator();
        int index = 0;
        for (Map.Entry<String,ArrayList<UMLClassPrototype>> entry : allData.entrySet()) {
             String key = entry.getKey();
             ArrayList<UMLClassPrototype> value = entry.getValue();
             index += value.size();
             System.out.println("P:" + key);
             for(UMLClassPrototype temp: value)
                System.out.println("C: " + temp.getClassName());
             System.out.println("----------------------------");
        }
        System.out.println("////////////////////////////");
        return index;
    }
    
    public void printLoad(){
           for (Map.Entry<String,ArrayList<UMLClassPrototype>> entry : allData.entrySet()) {
             String key = entry.getKey();
             ArrayList<UMLClassPrototype> value = entry.getValue();
             System.out.println("Package: " + key);
                System.out.println(value.toString());
                System.out.println("----------------------------");
             

        }
    }
    
    
    //check collision of classes in known package name
    public boolean isCollide(UMLClassPrototype umlC){
       int repeat = 0;
       ArrayList<UMLClassPrototype> arr = allData.get(umlC.getPackageName());
       for(UMLClassPrototype temp: arr){
           if (temp.getClassName().equals(umlC.getClassName()))
                repeat++;
       }
       if(repeat > 1){
        //   System.out.println("hahaha");
           return true;
       }else
           return false;
    }
    
    

    /**
     * @return the allData
     */
    public HashMap<String,ArrayList<UMLClassPrototype>> getAllData() {
        return allData;
    }

    /**
     * @param allData the allData to set
     */
    public void setAllData(HashMap<String,ArrayList<UMLClassPrototype>> allData) {
        this.allData = allData;
    }

 
    
}
