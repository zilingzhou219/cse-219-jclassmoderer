/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcm.data;

import java.util.ArrayList;

/**
 *
 * @author ziling
 */
public class UMLMethodPrototype extends UMLVariablePrototype{
//    String returnType = "";
    boolean isAbstract = false;
    ArrayList<String> argList = new ArrayList<>();
    public static String forImport = "";
    @Override
    public String toString(){
        String str = "";
        str += "MethodName: " + name +"\n"
                +"type: "+type+"\n"
                +"access: " + access+"\n"
                +"isAbstract: " + isAbstract+"\n"
                +"isStatic " + isStatic+"\n"
                +"argList: " + argList+"\n";
              return str;
    }
    public void forImport(){
        if(DataManager.allCoreClass.get(type) != null)
            forImport += DataManager.allCoreClass.get(type)+ "/n";
        for(int i = 0; i<argList.size(); i++){
            if(DataManager.allCoreClass.get(argList.get(i)) != null)
                forImport += DataManager.allCoreClass.get(argList.get(i))+ "/n";
        }
    }
    
    public String toMethodCode(){
        String str="";
        str = access;
        if(isAbstract){
            str += "abstract " + type +" " + name + "();";
            return str;
        }
        
        
        if(isStatic)
            str += " static";
        if(type.equals("constructor"))
            str += " " + name+"(";
        else
            str += " " + type +" " + name+"(";
        
        for(int i = 0; i< argList.size();i++){
            str += argList.get(i) + " Arg" + (i+1) +","; 
        }
        if(argList.size() > 0)
            str = str.substring(0,str.length() -1);
        
        str += ")" + "{" + "\n";
        if(type.equals("boolean"))
            str += "return false;\n";
        else if(type.equals("int"))
            str += "return 0;\n";
        else if(type.equals("double"))
            str += "return 0;\n";
        else if(type.equals("float"))
            str += "return 0;\n";
        else if(type.equals("byte"))
            str += "return 0;\n";
        else if(type.equals("short"))
            str += "return 0;\n";
        else if(type.equals("long"))
            str += "return 0;\n";
        else if(type.equals("char"))
            str += "return 'a';\n";
        else if(type.equals("void"))
            str +="\n";
        else if(type.equals("constructor"))
            str += "\n";
        else
            str += "return null;\n";
        str += "}";
        return str;
    }
    

    /**
     * @return the isAbstract
     */
    public boolean isIsAbstract() {
        return isAbstract;
    }

    /**
     * @param isAbstract the isAbstract to set
     */
    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    /**
     * @return the argList
     */
    public ArrayList<String> getArgList() {
        return argList;
    }

    /**
     * @param argList the argList to set
     */
    public void setArgList(ArrayList<String> argList) {
        this.argList = argList;
    }
}
