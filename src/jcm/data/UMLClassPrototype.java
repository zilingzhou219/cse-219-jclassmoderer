/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcm.data;

import static java.lang.Integer.max;
import static java.lang.Integer.min;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;

/**
 *
 * @author ziling
 */
public class UMLClassPrototype {
    
//    public static int totalInit = 0;
    ArrayList<UMLMethodPrototype> method = new ArrayList<>();
    ArrayList<UMLVariablePrototype> variable = new ArrayList<>();
    String className = "",parentName = "",packageName = "";
    boolean isIneterface = false;
    boolean isAbstract = false;
    boolean namingCollision = false;
    VBox container;
    VBox top,middle,bottom;
    double xLocation,yLocation,width,height;
    Polygon connector = new Polygon();
            
    public UMLClassPrototype(){
        className = "Default" + DataManager.totalInit;
        packageName = "Default" + DataManager.totalInit;
        xLocation = ThreadLocalRandom.current().nextInt(100, 1000);
        yLocation = ThreadLocalRandom.current().nextInt(100, 600);
        width = 150;
        height = 200;
//        xConnecter.add(xLocation + width/2);
//        yConnecter.add(yLocation);
    }
    
    public void initBox(){
        setContainer(new VBox());
        top = new VBox();
        top.setMinHeight(50);
        middle = new VBox();
        middle.setMinHeight(50);
        bottom = new VBox();
        bottom.setMinHeight(100);
        
        top.getChildren().add(new Label(className));
        getContainer().setLayoutX(getxLocation());
        getContainer().setLayoutY(getyLocation());

        getContainer().setMinHeight(height);
        getContainer().setMinWidth(width);
        if(isIneterface == false)
            container.getChildren().addAll(top,middle,bottom);
        else 
            container.getChildren().addAll(top,bottom);
        
        initBorderStyle();
    }
    public void addVariable(){
        
    }
    public void addMethod(){
        
    }
    public void fullFillLayout(){
        setContainer(new VBox());
        top = new VBox();
        top.setMinHeight(50);
        middle = new VBox();
        middle.setMinHeight(50);
        bottom = new VBox();
        bottom.setMinHeight(100);
        if(isAbstract){
            String abs = "{abstract}" +"\n"+ className;
            top.getChildren().add(new Label(abs));
        }else{
            top.getChildren().add(new Label(className));
        }
        
        getContainer().setLayoutX(getxLocation());
        getContainer().setLayoutY(getyLocation());

        getContainer().setMinHeight(height);
        getContainer().setMinWidth(width);
        
        if(isIneterface == false)
            container.getChildren().addAll(top,middle,bottom);
        else 
            container.getChildren().addAll(top,bottom);
        //need to add method and variable
        for(int i =0;i<variable.size(); i++){
            UMLVariablePrototype vp = variable.get(i);
            String oneV = "";
            if(vp.getAccess().equals("public"))
                oneV += "+";
            else if(vp.getAccess().equals("private"))
                oneV += "-";
            else if(vp.getAccess().equals("protected"))
                oneV += "#";
            
                     
            if(vp.isStatic)
                oneV+="$";
            oneV += vp.getName();
            if(vp.getType().equals("constructor"))
                oneV += ": " + "void";
            else
                oneV += ": " + vp.getType();
            
            middle.getChildren().add(new Label(oneV));
        }
        for(int i = 0; i<method.size();i++){
            UMLMethodPrototype mp = method.get(i);
            String oneM = "";
            if(mp.isAbstract)
                oneM += "{abstract}" + "\n";
            
            if(mp.getAccess().equals("public"))
                oneM += "+";
            else if(mp.getAccess().equals("private"))
                oneM += "-";
            else if(mp.getAccess().equals("protected"))
                oneM += "#";
            
            
            if(mp.isStatic)
                oneM +="$";
            
            oneM += mp.getName() +"(";
            for(int j = 0; j < mp.getArgList().size();j++){
                oneM += "Arg" + (j+1) + ": " + mp.getArgList().get(j) + ",";
            }
            if(mp.getArgList().size() > 0)
                oneM = oneM.substring(0, oneM.length() -1);
            
            oneM += ")";
            if(mp.getType().equals("constructor"))
                oneM += ": " + "void";
            else
                oneM += ": " + mp.getType();
            bottom.getChildren().add(new Label(oneM));
        }
        
        
        initBorderStyle();
    }
    public String toClassCode(){
        String str = "";
        str += "public";
        if(isAbstract)
            str += " " + "abstract class"; 
        else if(isIneterface)
            str += " interface";
        else
            str += " class";
        str += " " + className + "{\n\n";
        for(int i = 0; i < variable.size();i++){
            str += variable.get(i).toCode() + "\n";
        }
        for(int i = 0; i < method.size();i++){
            str += method.get(i).toMethodCode() +"\n";
        }
        str += "}";
        return str;
    }
    
    @Override
    public String toString(){
        String str = "";
        str += "className: " + className+ ""
                + "\n"
                + "packageName: " + packageName +"\n"
                + "parentName: " + parentName + "\n"
                + "isInterface: " + isIneterface + "\n"
                + "isAbstract: " + isAbstract+"\n"
                +"x: " + xLocation+"\n"
                +"y: " + yLocation + ""
                + "\n"
                +"height: " + height +"\n"
                +"width: " + width+""
                + "\n"
                +"connnector: " + connector.toString() +"\n";
        for(int i = 0;i< method.size();i++){
            str += method.get(i).toString();
        }
        for(int i = 0;i< variable.size();i++){
            str += variable.get(i).toString();
        }
        
        
         return str;
    }
    
    
    public void initBorderStyle(){
        getContainer().getStyleClass().add("uml_container_pane");
        top.getStyleClass().add("uml_pane");
        middle.getStyleClass().add("uml_pane");
        bottom.getStyleClass().add("uml_pane");
        
    }
    public void setPosition(double xloc,double yloc){
        xLocation = xloc;
        yLocation = yloc;
    }
    
    
    /**
     * @return the method
     */
    public ArrayList<UMLMethodPrototype> getMethod() {
        return method;
    }

    /**
     * @return the variable
     */
    public ArrayList<UMLVariablePrototype> getVariable() {
        return variable;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }


    /**
     * @return the parentName
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * @param parentName the parentName to set
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    /**
     * @return the isIneterface
     */
    public boolean isIsIneterface() {
        return isIneterface;
    }

    /**
     * @param isIneterface the isIneterface to set
     */
    public void setIsIneterface(boolean isIneterface) {
        this.isIneterface = isIneterface;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * @return the container
     */
    public VBox getContainer() {
        return container;
    }

    /**
     * @param container the container to set
     */
    public void setContainer(VBox container) {
        this.container = container;
    }

    /**
     * @return the xLocation
     */
    public double getxLocation() {
        return xLocation;
    }

    /**
     * @param xLocation the xLocation to set
     */
    public void setxLocation(double xLocation) {
        this.xLocation = xLocation;
    }

    /**
     * @return the yLocation
     */
    public double getyLocation() {
        return yLocation;
    }

    /**
     * @param yLocation the yLocation to set
     */
    public void setyLocation(double yLocation) {
        this.yLocation = yLocation;
    }

    /**
     * @return the top
     */
    public VBox getTop() {
        return top;
    }

    /**
     * @return the middle
     */
    public VBox getMiddle() {
        return middle;
    }

    /**
     * @return the bottom
     */
    public VBox getBottom() {
        return bottom;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the namingCollision
     */
    public boolean isNamingCollision() {
        return namingCollision;
    }

    /**
     * @param namingCollision the namingCollision to set
     */
    public void setNamingCollision(boolean namingCollision) {
        this.namingCollision = namingCollision;
    }

    /**
     * @return the isAbstract
     */
    public boolean isIsAbstract() {
        return isAbstract;
    }

    /**
     * @param isAbstract the isAbstract to set
     */
    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    /**
     * @return the connector
     */
    public Polygon getConnector() {
        return connector;
    }

    /**
     * @param connector the connector to set
     */
    public void setConnector(Polygon connector) {
        this.connector = connector;
    }



}
