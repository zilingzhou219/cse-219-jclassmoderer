/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import java.util.ArrayList;
import jcm.data.UMLClassPrototype;

/**
 *
 * @author ziling
 */
public class DriverAddInterface {
     public ArrayList<UMLClassPrototype> addInterface(){
        Driver driver = new Driver();
        ArrayList<UMLClassPrototype> arrl = driver.getRecitation6();
        UMLClassPrototype inter = new UMLClassPrototype();
        inter.setClassName("Inter");
        inter.setPackageName("Recitation6");
        inter.setIsIneterface(true);
        inter.setIsAbstract(false);
        inter.setParentName("");
        arrl.add(inter);
//        abs.getMethod().add(DataTaskM2);
//        abs.getMethod().add(DataTaskM1);
//        abs.getVariable().add(DataTaskV1);
//        abs.getVariable().add(DataTaskV2);
        return arrl;
    }
}
