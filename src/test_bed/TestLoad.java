/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import jcm.data.DataManager;
import jcm.file.FileManager;

/**
 *
 * @author ziling
 */
public class TestLoad {
    public static void main(String[] args) throws Exception{
        FileManager fm = new FileManager();
        DataManager dm = new DataManager();
        fm.loadData(dm, "./work/DesignTestSave.json");
        dm.printLoad();
    }
}
