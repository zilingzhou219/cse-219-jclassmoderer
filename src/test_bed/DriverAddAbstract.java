/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import java.util.ArrayList;
import jcm.data.UMLClassPrototype;

/**
 *
 * @author ziling
 */
public class DriverAddAbstract {
    public ArrayList<UMLClassPrototype> addAbstract(){
        Driver driver = new Driver();
        ArrayList<UMLClassPrototype> arrl = driver.getRecitation6();
        UMLClassPrototype abs = new UMLClassPrototype();
        abs.setClassName("Abs");
        abs.setPackageName("Recitation6");
        abs.setIsIneterface(false);
        abs.setIsAbstract(true);
        abs.setParentName("");
        arrl.add(abs);
        
       
//        abs.getMethod().add(DataTaskM2);
//        abs.getMethod().add(DataTaskM1);
//        abs.getVariable().add(DataTaskV1);
//        abs.getVariable().add(DataTaskV2);
        return arrl;
    }
}
