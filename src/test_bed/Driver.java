/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import java.util.ArrayList;
import jcm.data.UMLClassPrototype;
import jcm.data.UMLMethodPrototype;
import jcm.data.UMLVariablePrototype;

/**
 *
 * @author ziling
 */
public class Driver {
    public ArrayList<UMLClassPrototype> getRecitation6(){
        UMLMethodPrototype counterTaskM1 = new UMLMethodPrototype();
        counterTaskM1.setName("call");
        counterTaskM1.setType("void");
        counterTaskM1.setIsStatic(true);
        counterTaskM1.setIsAbstract(false);
        counterTaskM1.setAccess("protected");

        UMLMethodPrototype counterTaskM2 = new UMLMethodPrototype();
        counterTaskM2.setName("CounterTask");
        counterTaskM2.setType("constructor");
        counterTaskM2.setIsStatic(false);
        counterTaskM2.setIsAbstract(false);
        counterTaskM2.setAccess("public");
        counterTaskM2.getArgList().add("ThreadExample");

        UMLVariablePrototype counterTaskV1 = new UMLVariablePrototype();
        counterTaskV1.setName("app");
        counterTaskV1.setType("ThreadExample");
        counterTaskV1.setIsStatic(false);
        counterTaskV1.setAccess("private");

        UMLVariablePrototype counterTaskV2 = new UMLVariablePrototype();
        counterTaskV2.setName("counter");
        counterTaskV2.setType("int");
        counterTaskV2.setIsStatic(false);
        counterTaskV2.setAccess("private");

        UMLClassPrototype counterTask = new UMLClassPrototype();
        counterTask.setClassName("CounterTask");
        counterTask.setPackageName("Recitation6");
        counterTask.setIsIneterface(false);
        counterTask.setIsAbstract(false);
        counterTask.setParentName("");
        counterTask.getMethod().add(counterTaskM2);
        counterTask.getMethod().add(counterTaskM1);
        counterTask.getVariable().add(counterTaskV1);
        counterTask.getVariable().add(counterTaskV2);
        //---------------------------------------------------------------------------------------------

        UMLMethodPrototype DataTaskM1 = new UMLMethodPrototype();
        DataTaskM1.setName("call");
        DataTaskM1.setType("void");
        DataTaskM1.setIsStatic(true);
        DataTaskM1.setIsAbstract(false);
        DataTaskM1.setAccess("protected");

        UMLMethodPrototype DataTaskM2 = new UMLMethodPrototype();
        DataTaskM2.setName("DataTask");
        DataTaskM2.setType("constructor");
        DataTaskM2.setIsStatic(false);
        DataTaskM2.setIsAbstract(false);
        DataTaskM2.setAccess("public");
        DataTaskM2.getArgList().add("ThreadExample");

        UMLVariablePrototype DataTaskV1 = new UMLVariablePrototype();
        DataTaskV1.setName("app");
        DataTaskV1.setType("ThreadExample");
        DataTaskV1.setIsStatic(false);
        DataTaskV1.setAccess("private");

        UMLVariablePrototype DataTaskV2 = new UMLVariablePrototype();
        DataTaskV2.setName("now");
        DataTaskV2.setType("Date");
        DataTaskV2.setIsStatic(false);
        DataTaskV2.setAccess("private");

        UMLClassPrototype DataTask = new UMLClassPrototype();
        DataTask.setClassName("DataTask");
        DataTask.setPackageName("Recitation6");
        DataTask.setIsIneterface(false);
        DataTask.setIsAbstract(false);
        DataTask.setParentName("");
        DataTask.getMethod().add(DataTaskM2);
        DataTask.getMethod().add(DataTaskM1);
        DataTask.getVariable().add(DataTaskV1);
        DataTask.getVariable().add(DataTaskV2);

        //--------------------------------------------------------------    
        //--------------------------------------------------------------
        UMLMethodPrototype PauseHandlerM1 = new UMLMethodPrototype();
        PauseHandlerM1.setName("handle");
        PauseHandlerM1.setType("void");
        PauseHandlerM1.setIsStatic(false);
        PauseHandlerM1.setIsAbstract(false);
        PauseHandlerM1.setAccess("public");

        UMLMethodPrototype PauseHandlerM2 = new UMLMethodPrototype();
        PauseHandlerM2.setName("PauseHandler");
        PauseHandlerM2.setType("constructor");
        PauseHandlerM2.setIsStatic(false);
        PauseHandlerM2.setIsAbstract(false);
        PauseHandlerM2.setAccess("public");
        PauseHandlerM2.getArgList().add("ThreadExample");

        UMLVariablePrototype PauseHandlerV1 = new UMLVariablePrototype();
        PauseHandlerV1.setName("app");
        PauseHandlerV1.setType("ThreadExample");
        PauseHandlerV1.setIsStatic(false);
        PauseHandlerV1.setAccess("private");

        UMLClassPrototype PauseHandler = new UMLClassPrototype();
        PauseHandler.setClassName("PauseHandler");
        PauseHandler.setPackageName("Recitation6");
        PauseHandler.setIsIneterface(false);
        PauseHandler.setIsAbstract(false);
        PauseHandler.setParentName("");
        PauseHandler.getMethod().add(PauseHandlerM2);
        PauseHandler.getMethod().add(PauseHandlerM1);
        PauseHandler.getVariable().add(PauseHandlerV1);

        //--------------------------------------------------------------    
        //-------------------------------------------------------------- 
        UMLMethodPrototype StartHandlerM1 = new UMLMethodPrototype();
        StartHandlerM1.setName("handle");
        StartHandlerM1.setType("void");
        StartHandlerM1.setIsStatic(false);
        StartHandlerM1.setIsAbstract(false);
        StartHandlerM1.setAccess("public");

        UMLMethodPrototype StartHandlerM2 = new UMLMethodPrototype();
        StartHandlerM2.setName("StartHandler");
        StartHandlerM2.setType("constructor");
        StartHandlerM2.setIsStatic(false);
        StartHandlerM2.setIsAbstract(false);
        StartHandlerM2.setAccess("public");
        StartHandlerM2.getArgList().add("ThreadExample");

        UMLVariablePrototype StartHandlerV1 = new UMLVariablePrototype();
        StartHandlerV1.setName("app");
        StartHandlerV1.setType("ThreadExample");
        StartHandlerV1.setIsStatic(false);
        StartHandlerV1.setAccess("private");

        UMLClassPrototype StartHandler = new UMLClassPrototype();
        StartHandler.setClassName("StartHandler");
        StartHandler.setPackageName("Recitation6");
        StartHandler.setIsIneterface(false);
        StartHandler.setIsAbstract(false);
        StartHandler.setParentName("");
        StartHandler.getMethod().add(StartHandlerM2);
        StartHandler.getMethod().add(StartHandlerM1);
        StartHandler.getVariable().add(StartHandlerV1);

        //--------------------------------------------------------------    
        //-------------------------------------------------------------- 
        UMLMethodPrototype ThreadExampleM1 = new UMLMethodPrototype();
        ThreadExampleM1.setName("start");
        ThreadExampleM1.setType("void");
        ThreadExampleM1.setIsStatic(false);
        ThreadExampleM1.setIsAbstract(false);
        ThreadExampleM1.setAccess("public");
        ThreadExampleM1.getArgList().add("Stage");

        UMLMethodPrototype ThreadExampleM2 = new UMLMethodPrototype();
        ThreadExampleM2.setName("startWork");
        ThreadExampleM2.setType("void");
        ThreadExampleM2.setIsStatic(false);
        ThreadExampleM2.setIsAbstract(false);
        ThreadExampleM2.setAccess("public");
        //ThreadExampleM2.getArgList().add("ThreadExample");
        UMLMethodPrototype ThreadExampleM3 = new UMLMethodPrototype();
        ThreadExampleM3.setName("pauseWork");
        ThreadExampleM3.setType("void");
        ThreadExampleM3.setIsStatic(false);
        ThreadExampleM3.setIsAbstract(false);
        ThreadExampleM3.setAccess("public");

        UMLMethodPrototype ThreadExampleM4 = new UMLMethodPrototype();
        ThreadExampleM4.setName("doWork");
        ThreadExampleM4.setType("boolean");
        ThreadExampleM4.setIsStatic(false);
        ThreadExampleM4.setIsAbstract(false);
        ThreadExampleM4.setAccess("public");

        UMLMethodPrototype ThreadExampleM5 = new UMLMethodPrototype();
        ThreadExampleM5.setName("appendText");
        ThreadExampleM5.setType("void");
        ThreadExampleM5.setIsStatic(false);
        ThreadExampleM5.setIsAbstract(false);
        ThreadExampleM5.setAccess("public");
        ThreadExampleM5.getArgList().add("String");

        UMLMethodPrototype ThreadExampleM6 = new UMLMethodPrototype();
        ThreadExampleM6.setName("sleep");
        ThreadExampleM6.setType("void");
        ThreadExampleM6.setIsStatic(false);
        ThreadExampleM6.setIsAbstract(false);
        ThreadExampleM6.setAccess("public");
        ThreadExampleM6.getArgList().add("int");

        UMLMethodPrototype ThreadExampleM7 = new UMLMethodPrototype();
        ThreadExampleM7.setName("initLayout");
        ThreadExampleM7.setType("void");
        ThreadExampleM7.setIsStatic(false);
        ThreadExampleM7.setIsAbstract(false);
        ThreadExampleM7.setAccess("private");

        UMLMethodPrototype ThreadExampleM8 = new UMLMethodPrototype();
        ThreadExampleM8.setName("initHandler");
        ThreadExampleM8.setType("void");
        ThreadExampleM8.setIsStatic(false);
        ThreadExampleM8.setIsAbstract(false);
        ThreadExampleM8.setAccess("private");

        UMLMethodPrototype ThreadExampleM9 = new UMLMethodPrototype();
        ThreadExampleM9.setName("initWindow");
        ThreadExampleM9.setType("void");
        ThreadExampleM9.setIsStatic(false);
        ThreadExampleM9.setIsAbstract(false);
        ThreadExampleM9.setAccess("private");
        ThreadExampleM9.getArgList().add("Stage");

        UMLMethodPrototype ThreadExampleM10 = new UMLMethodPrototype();
        ThreadExampleM10.setName("initThread");
        ThreadExampleM10.setType("void");
        ThreadExampleM10.setIsStatic(false);
        ThreadExampleM10.setIsAbstract(false);
        ThreadExampleM10.setAccess("private");

        UMLMethodPrototype ThreadExampleM11 = new UMLMethodPrototype();
        ThreadExampleM11.setName("main");
        ThreadExampleM11.setType("void");
        ThreadExampleM11.setIsStatic(true);
        ThreadExampleM11.setIsAbstract(false);
        ThreadExampleM11.setAccess("public");
        ThreadExampleM11.getArgList().add("String[]");
        //TODO decide final or not for String
        UMLVariablePrototype ThreadExampleV1 = new UMLVariablePrototype();
        ThreadExampleV1.setName("START_TEXT");
        ThreadExampleV1.setType("String");
        ThreadExampleV1.setIsStatic(true);
        ThreadExampleV1.setAccess("public");

        UMLVariablePrototype ThreadExampleV2 = new UMLVariablePrototype();
        ThreadExampleV2.setName("PAUSE_TEXT");
        ThreadExampleV2.setType("String");
        ThreadExampleV2.setIsStatic(true);
        ThreadExampleV2.setAccess("public");

        UMLVariablePrototype ThreadExampleV3 = new UMLVariablePrototype();
        ThreadExampleV3.setName("window");
        ThreadExampleV3.setType("Stage");
        ThreadExampleV3.setIsStatic(false);
        ThreadExampleV3.setAccess("private");

        UMLVariablePrototype ThreadExampleV4 = new UMLVariablePrototype();
        ThreadExampleV4.setName("appPane");
        ThreadExampleV4.setType("BorderPane");
        ThreadExampleV4.setIsStatic(false);
        ThreadExampleV4.setAccess("private");

        UMLVariablePrototype ThreadExampleV5 = new UMLVariablePrototype();
        ThreadExampleV5.setName("topPane");
        ThreadExampleV5.setType("FlowPane");
        ThreadExampleV5.setIsStatic(false);
        ThreadExampleV5.setAccess("private");

        UMLVariablePrototype ThreadExampleV6 = new UMLVariablePrototype();
        ThreadExampleV6.setName("startButton");
        ThreadExampleV6.setType("Button");
        ThreadExampleV6.setIsStatic(false);
        ThreadExampleV6.setAccess("private");

        UMLVariablePrototype ThreadExampleV7 = new UMLVariablePrototype();
        ThreadExampleV7.setName("pauseButton");
        ThreadExampleV7.setType("Button");
        ThreadExampleV7.setIsStatic(false);
        ThreadExampleV7.setAccess("private");

        UMLVariablePrototype ThreadExampleV8 = new UMLVariablePrototype();
        ThreadExampleV8.setName("scrollPane");
        ThreadExampleV8.setType("ScrollPane");
        ThreadExampleV8.setIsStatic(false);
        ThreadExampleV8.setAccess("private");

        UMLVariablePrototype ThreadExampleV9 = new UMLVariablePrototype();
        ThreadExampleV9.setName("textArea");
        ThreadExampleV9.setType("TextArea");
        ThreadExampleV9.setIsStatic(false);
        ThreadExampleV9.setAccess("private");

        UMLVariablePrototype ThreadExampleV10 = new UMLVariablePrototype();
        ThreadExampleV10.setName("dataThread");
        ThreadExampleV10.setType("Thread");
        ThreadExampleV10.setIsStatic(false);
        ThreadExampleV10.setAccess("private");

        UMLVariablePrototype ThreadExampleV11 = new UMLVariablePrototype();
        ThreadExampleV11.setName("dataTask");
        ThreadExampleV11.setType("Task");
        ThreadExampleV11.setIsStatic(false);
        ThreadExampleV11.setAccess("private");

        UMLVariablePrototype ThreadExampleV12 = new UMLVariablePrototype();
        ThreadExampleV12.setName("counterThread");
        ThreadExampleV12.setType("Thread");
        ThreadExampleV12.setIsStatic(false);
        ThreadExampleV12.setAccess("private");

        UMLVariablePrototype ThreadExampleV13 = new UMLVariablePrototype();
        ThreadExampleV13.setName("counterTask");
        ThreadExampleV13.setType("Task");
        ThreadExampleV13.setIsStatic(false);
        ThreadExampleV13.setAccess("private");

        UMLVariablePrototype ThreadExampleV14 = new UMLVariablePrototype();
        ThreadExampleV14.setName("work");
        ThreadExampleV14.setType("boolean");
        ThreadExampleV14.setIsStatic(false);
        ThreadExampleV14.setAccess("private");

        UMLClassPrototype ThreadExample = new UMLClassPrototype();
        ThreadExample.setClassName("ThreadExample");
        ThreadExample.setPackageName("Recitation6");
        ThreadExample.setIsIneterface(false);
        ThreadExample.setIsAbstract(false);
        ThreadExample.setParentName("Application");
        ThreadExample.getMethod().add(ThreadExampleM1);
        ThreadExample.getMethod().add(ThreadExampleM2);
        ThreadExample.getMethod().add(ThreadExampleM3);
        ThreadExample.getMethod().add(ThreadExampleM4);
        ThreadExample.getMethod().add(ThreadExampleM5);
        ThreadExample.getMethod().add(ThreadExampleM6);
        ThreadExample.getMethod().add(ThreadExampleM7);
        ThreadExample.getMethod().add(ThreadExampleM8);
        ThreadExample.getMethod().add(ThreadExampleM9);
        ThreadExample.getMethod().add(ThreadExampleM10);
        ThreadExample.getMethod().add(ThreadExampleM11);

        ThreadExample.getVariable().add(ThreadExampleV1);
        ThreadExample.getVariable().add(ThreadExampleV2);
        ThreadExample.getVariable().add(ThreadExampleV3);
        ThreadExample.getVariable().add(ThreadExampleV4);
        ThreadExample.getVariable().add(ThreadExampleV5);
        ThreadExample.getVariable().add(ThreadExampleV6);
        ThreadExample.getVariable().add(ThreadExampleV7);
        ThreadExample.getVariable().add(ThreadExampleV8);
        ThreadExample.getVariable().add(ThreadExampleV9);
        ThreadExample.getVariable().add(ThreadExampleV10);
        ThreadExample.getVariable().add(ThreadExampleV11);
        ThreadExample.getVariable().add(ThreadExampleV12);
        ThreadExample.getVariable().add(ThreadExampleV13);
        ThreadExample.getVariable().add(ThreadExampleV14);

        //--------------------------------------------------------------    
        //--------------------------------------------------------------            
        UMLMethodPrototype ApplicationM1 = new UMLMethodPrototype();
        ApplicationM1.setName("initThread");
        ApplicationM1.setType("void");
        ApplicationM1.setIsStatic(false);
        ApplicationM1.setIsAbstract(true);
        ApplicationM1.setAccess("");

        UMLClassPrototype Application = new UMLClassPrototype();
        Application.setClassName("Aplication");
        Application.setPackageName("Recitation6");
        Application.setIsIneterface(false);
        Application.setIsAbstract(true);
        Application.setParentName("");
        Application.getMethod().add(ApplicationM1);
        double x = ThreadExample.getxLocation()+ThreadExample.getWidth()/2;
        double y = ThreadExample.getyLocation();
        double xf = Application.getxLocation() + Application.getWidth();
        double yf = Application.getyLocation();
        
        ThreadExample.getConnector().getPoints().addAll(x,y,xf,yf);
        
        ArrayList<UMLClassPrototype> list = new ArrayList<>();
        list.add(counterTask);
        list.add(DataTask);
        list.add(PauseHandler);
        list.add(StartHandler);
        list.add(ThreadExample);
        list.add(Application);
        
        return list;
    }
    
}
