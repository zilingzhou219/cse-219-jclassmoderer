/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuite;

import java.util.ArrayList;
import java.util.HashMap;
import jcm.data.DataManager;
import jcm.data.UMLClassPrototype;
import jcm.data.UMLMethodPrototype;
import jcm.data.UMLVariablePrototype;
import jcm.file.FileManager;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import test_bed.Driver;

/**
 *
 * @author ziling
 */
public class JUnitTest1 extends TestCase{
    
    public JUnitTest1() {
    }
    
   @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
  //      System.out.println("* UtilsJUnit3Test: setUp() method");
    }
    
    @After
    public void tearDown() throws Exception{
        super.tearDown();
  //      System.out.println("* UtilsJUnit3Test: tearDown() method");
    }
    
    public void testEqual()throws Exception{
        Driver driver = new Driver();
        ArrayList<UMLClassPrototype> umlCL = driver.getRecitation6();
        DataManager dm = new DataManager();
        dm.getAllData().put("Recitation6", umlCL);
        FileManager fm = new FileManager();
        fm.saveData(dm,"work/DesignTestSuite1");
        
        DataManager dm2 = new DataManager();
        fm.loadData(dm2,"work/DesignTestSuite1.json");
        
        HashMap<String,ArrayList<UMLClassPrototype>> hm1 = dm.getAllData();
        HashMap<String,ArrayList<UMLClassPrototype>> hm2 = dm2.getAllData();
        ArrayList<UMLClassPrototype> list2 = hm2.get("Recitation6");
        
        assertEquals(umlCL.get(0).getxLocation(),list2.get(0).getxLocation() );
        assertEquals(umlCL.get(0).getyLocation(),list2.get(0).getyLocation() );
        assertEquals(umlCL.get(0).getxLocation(),list2.get(0).getxLocation() );
        assertEquals(umlCL.get(0).getClassName(),list2.get(0).getClassName() );
        assertEquals(umlCL.get(0).getConnector().getPoints(),list2.get(0).getConnector().getPoints());
        
        UMLMethodPrototype oriM = umlCL.get(0).getMethod().get(0);
        UMLMethodPrototype oriM2 = umlCL.get(1).getMethod().get(0);
        UMLMethodPrototype loadM = list2.get(0).getMethod().get(0);
        UMLMethodPrototype loadM2 = list2.get(1).getMethod().get(0);
        
        assertEquals(oriM.getArgList(),loadM.getArgList());
        assertEquals(oriM.getType(),loadM.getType());
        assertEquals(oriM.getName(),loadM.getName());
        //assertEquals(oriM)
        
        assertEquals(oriM2.getArgList(),loadM2.getArgList());
        assertEquals(oriM2.getType(),loadM2.getType());
        assertEquals(oriM2.getName(),loadM2.getName());
        
        UMLVariablePrototype oldV = umlCL.get(0).getVariable().get(0);
        UMLVariablePrototype loadV = umlCL.get(0).getVariable().get(0);
        assertEquals(oldV.getName(),loadV.getName());
        assertEquals(oldV.getType(), loadV.getType());
        
        
        
    }

    
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
